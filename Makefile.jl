# Load custom scripts

using Revise
using ComposerTools

# Update Manifest.toml and Project.toml

copyproject()

# Create scripts by merging input scripts inside each folder

createscripts(joinpath("..", "..", "scripts", "code"), "scripts", remove = true)

# Copy functions folder

# cp(joinpath("..", "..", "src"), joinpath("src"), force = true)

# Copy data folder

# cp(joinpath("..", "..", "data"), joinpath("data"), force = true)

# Compile scripts to notebooks

createnotebooks("scripts", "notebooks")
