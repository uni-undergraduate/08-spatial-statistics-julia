import CairoMakie as MK
import DataFrames as DF
using AlgebraOfGraphics

# ### Visualize simulations

# visualize simulations
function vissim(df::DF.DataFrame, x::Symbol, y::Symbol;
    layout = false, linkaxis = true, onlylines = false, linewidth = 1, markersize = 10)

    # layers
    if onlylines
        layers = mapping(color = :Realization) * visual(MK.Lines, linewidth = linewidth)
    else
        layers = visual(MK.Lines, color = :gray, linestyle = :dash, linewidth = linewidth) +
        mapping(color = :Realization) * visual(MK.Scatter, markersize = markersize)
    end
    fig = data(df) * mapping(x, y) * layers

    # facet
    if layout
        fig = fig * mapping(layout = :Realization)
    else
        fig = fig * mapping(group = :Realization)
    end

    # axis links
    if linkaxis
        draw(fig)
    else
        draw(fig, facet = (; linkxaxes = :none, linkyaxes = :none))
    end
end

# transform simulation matrix to dataframe
function mat2df(X::AbstractMatrix)
    n = size(X, 2)
    T = size(X, 1)
    d = DF.DataFrame(X, string.(1:n))
    DF.insertcols!(d, :t => 1:T)
    df = DF.stack(d, 1:n, variable_name = :Realization, value_name = "X(t)")
    DF.transform!(df, :Realization => (x -> replace.(x, "x" => "")) => :Realization)
end

function mat2df(X::AbstractMatrix, Y::AbstractMatrix)
    n = size(X, 2)
    T = size(X, 1)
    df = DF.DataFrame(
        t = repeat(1:T, n),
        Realization = repeat(string.(1:n), inner = T),
        x = vec(X), y = vec(Y)
    )
    DF.rename!(df, :x => "X(t)", :y => "Y(t)")
    df
end
