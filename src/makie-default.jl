import CairoMakie as MK

# ### Makie Theme

ggplot_theme = MK.Theme(
    Axis = (
        backgroundcolor = :gray90,
        # backgroundcolor = :white,
        leftspinevisible = false,
        rightspinevisible = false,
        bottomspinevisible = false,
        topspinevisible = false,
        xgridcolor = :white,
        ygridcolor = :white,
    )
)

MK.set_theme!(ggplot_theme)

